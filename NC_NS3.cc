/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/netanim-module.h"
#include "my-tag.h"
#include "udp-echo-helper-r.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <regex>
#include <string>
#include <algorithm>
#include <sys/stat.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("NetworkCoding");

std::vector<std::vector<int>> ffGraph;
std::vector<int> destNodes;
std::vector<int> codNodes;
std::vector<int> srcPackages;

NodeContainer ncNodes; //nodos del grafo
NodeContainer tNodes;  //nodos terminales que se añaden a Source y Dests.

std::vector<std::vector<NetDeviceContainer>> ncDevices;
std::vector<NetDeviceContainer> tDevices;

std::vector<std::vector<Ipv4InterfaceContainer>> ncInterfaces;
std::vector<Ipv4InterfaceContainer> tInterfaces;

std::string dirPath;

Ipv4Address multicastSource;
Ipv4Address multicastGroup ("225.1.2.4");
uint16_t multicastPort = 9;

std::vector<std::vector<std::string>> analysis;

int getElemPosV(std::vector<int> V, int elem){
	return (find(V.begin(), V.end(), elem)) - V.begin();
}

uint8_t pktCount = 0;
void SourceUdpTxTrace(Ptr<const Packet> pkt){
	//idTag -> n = flow
	uint8_t tag;
	if(pktCount % 2 == 0){ //bbbbbb01
		tag = 1;
	}else{                 //bbbbbb10
		tag = 2;
	}
	pktCount = pktCount + 1;

	MyTag idTag;
	idTag.SetSimpleValue(tag);
	pkt -> AddPacketTag(idTag);
}


std::vector<long int> srcRxPkt;
bool srcFirst = true;
uint32_t srcAIndex = 0;
void SourceSinkRxTrace(Ptr<const Packet> pkt, const Address &addr){
	std::ofstream srcLog;
	if(srcFirst){
		srcLog.open(dirPath+"/source.plg", std::ofstream::trunc);
    srcLog << "Paquetes enviados de Source:" << std::endl << std::endl;
		srcFirst = false;
	}else{
		srcLog.open(dirPath+"/source.plg", std::ofstream::app);
	}
	analysis.push_back(std::vector<std::string>(2 + destNodes.size(),""));

	//tipo del paquete A|B
	uint8_t pktC;
	MyTag idTag;
	if(pkt -> PeekPacketTag(idTag)){
		pktC = idTag.GetSimpleValue();
	}

	//pktContent
	uint8_t* buffer = new uint8_t[pkt->GetSize()];
	pkt -> CopyData (buffer, pkt->GetSize());
	srcRxPkt.push_back(atol((char*)buffer));
	double timeNow = Simulator::Now().GetSeconds();
	srcLog << "at time " << timeNow << "| pkt: ";
	srcLog << srcRxPkt.back() << std::endl;
	srcLog.close();

	analysis.at(srcAIndex).at(0) = std::to_string(srcRxPkt.back());
	analysis.at(srcAIndex).at(1) = std::to_string(timeNow);
	srcAIndex = srcAIndex + 1;

	Ptr<Socket> srcSocket = Socket::CreateSocket(ncNodes.Get(0),
																							UdpSocketFactory::GetTypeId());
	srcSocket -> Connect(InetSocketAddress(multicastGroup, multicastPort));

	std::ostringstream msgx;
	MyTag outTag;
	long int pktSrc = srcRxPkt.back();
	outTag.SetSimpleValue(pktC);

	msgx << pktSrc;
	Ptr<Packet> outPkt = Create<Packet>((uint8_t*) msgx.str().c_str(), pkt -> GetSize());
	outPkt -> AddPacketTag(outTag);

	Ptr<Node> src = ncNodes.Get(0);
	Ipv4StaticRoutingHelper multicast;
	for(uint32_t i = 0; i < srcPackages.size(); i++){
		if((uint8_t)srcPackages.at(i) == pktC){
			Ptr<NetDevice> outIf = ncDevices.at(0).at(i + 1).Get(0);
		  multicast.SetDefaultMulticastRoute (src, outIf);
			srcSocket -> Send(outPkt, 0);
		}
	}

	//if there's a A+B in src.
	if(srcRxPkt.size() == 2 && getElemPosV(srcPackages, 0x3) != (int)srcPackages.size()){
		std::ostringstream msgx2;
		MyTag outTag2;
		long int pktSrc = srcRxPkt.at(0) ^ srcRxPkt.at(1);
		srcRxPkt.erase(srcRxPkt.begin(), srcRxPkt.begin() + 2);

		msgx2 << pktSrc;
		outTag2.SetSimpleValue(0x3);

		outPkt = Create<Packet>((uint8_t*) msgx2.str().c_str(), pkt -> GetSize());
		outPkt -> AddPacketTag(outTag2);

		for(uint32_t i = 0; i < srcPackages.size(); i++){
			if((uint8_t)srcPackages.at(i) == 0x3){
				Ptr<NetDevice> outIf = ncDevices.at(0).at(i + 1).Get(0);
			  multicast.SetDefaultMulticastRoute (src, outIf);
				srcSocket -> Send(outPkt, 0);
			}
		}
	}

	srcSocket -> Close();

	Ptr<Ipv4> ipv4Src = src -> GetObject<Ipv4>();
	Ptr<Ipv4StaticRouting> ipv4SR = multicast.GetStaticRouting(ipv4Src);
	ipv4SR -> RemoveMulticastRoute(ipv4SR -> GetNMulticastRoutes() - 1);
}

std::vector<std::vector<std::vector<long int>>> codRxPkt;
std::vector<std::vector<int>> codRxFlags;
std::vector<bool> codFirst;
void CodSinkRxTrace(std::string context, Ptr<const Packet> pkt, const Address &addr){

	std::regex rgx("(\\d+)");
	std::smatch match;
	uint32_t codNode = 0; //codNode that made Callback
	if(std::regex_search(context, match, rgx)){
		codNode =  std::stoi(match[1]);                //codNode #
		codNode = getElemPosV(codNodes, codNode); //change to pos in codNodes
	}

	std::ofstream codLog;
	if(codFirst.at(codNode)){
		codLog.open(dirPath+"/codificador_" + std::to_string(codNode) +".plg", std::ofstream::trunc);
    codLog << "Trafico de Paquetes en Codificador #" << codNode << ":" <<
																									std::endl << std::endl;
		codFirst.at(codNode) = false;
	}else{
		codLog.open(dirPath+"/codificador_" + std::to_string(codNode) +".plg", std::ofstream::app);
	}

	uint8_t pktC; //flag from pkt
	MyTag idTag;
	if(pkt -> PeekPacketTag(idTag)){
		pktC = idTag.GetSimpleValue();
	}

	//pktContent
	uint8_t* buffer = new uint8_t[pkt->GetSize()];
	pkt -> CopyData (buffer, pkt->GetSize());

	int codFlagPos = getElemPosV(codRxFlags.at(codNode), pktC);
	if(codFlagPos == (int)codRxFlags.at(codNode).size()){
		codRxFlags.at(codNode).push_back((int)pktC);
	}
	//save pkt content in one of the vectors of codBuffer
	codRxPkt.at(codNode).at(codFlagPos).push_back(atol((char*)buffer));

	bool checkPkt = true;
	//Checks if there's a stored pkt for each in IF
	for(uint32_t i = 0; i < codRxPkt.at(codNode).size(); i++){
		if(codRxPkt.at(codNode).at(i).size() == 0){ //no hay paquetes con flag i
			checkPkt = false;
		}
	}

	if(checkPkt){
		std::ostringstream msgx;
		MyTag outTag;

		//take and xOr the first packet from each buffer
		codLog << "Llegan los paquetes: " << std::endl;
		long int pktCod = codRxPkt.at(codNode).at(0).at(0);
		codLog << codRxPkt.at(codNode).at(0).at(0) << std::endl;
		codRxPkt.at(codNode).at(0).erase(codRxPkt.at(codNode).at(0).begin());

		uint8_t tag = codRxFlags.at(codNode).at(0);
		for(uint32_t i = 1; i < codRxPkt.at(codNode).size(); i++){
			pktCod = pktCod ^ codRxPkt.at(codNode).at(i).at(0);
			codLog << codRxPkt.at(codNode).at(i).at(0) << std::endl;
			codRxPkt.at(codNode).at(i).erase(codRxPkt.at(codNode).at(i).begin());


			tag = tag ^ codRxFlags.at(codNode).at(i);
		}
		msgx << pktCod;
		outTag.SetSimpleValue(tag);

		//log
		codLog << "Sale el paquete: " << std::endl;
		codLog << pktCod << std::endl << std::endl;
		codLog.close();

		Ptr<Node> src = ncNodes.Get(codNodes.at(codNode));

		Ipv4StaticRoutingHelper multicast;
		Ptr<Ipv4> ipv4Src = src -> GetObject<Ipv4>();
		Ptr<Ipv4StaticRouting> ipv4SR = multicast.GetStaticRouting(ipv4Src);

		/*std::vector<uint32_t> outputDevices;
		for(uint32_t i = 0; i < ffGraph.size(); i++){
			if(ffGraph.at(codNodes.at(codNode)).at(i) == 1){
				Ptr<NetDevice> outIf = ncDevices.at(codNodes.at(codNode)).at(i).Get(0);
				std::cout << "outif=" << outIf -> GetIfIndex() << std::endl;
			  outputDevices.push_back(outIf -> GetIfIndex());
			}
		}
		ipv4SR -> AddMulticastRoute (Ipv4Address::GetAny(), Ipv4Address::GetAny(), Ipv4::IF_ANY,
			 													outputDevices);*/

		Ptr<Packet> outPkt = Create<Packet>((uint8_t*) msgx.str().c_str(), pkt -> GetSize());
		outPkt -> AddPacketTag(outTag);
		Ptr<Socket> srcSocket = Socket::CreateSocket(ncNodes.Get(codNodes.at(codNode)),
		 																						UdpSocketFactory::GetTypeId());
		srcSocket -> Connect(InetSocketAddress(multicastGroup, multicastPort));

		for(uint32_t i = 0; i < ffGraph.size(); i++){
			if(ffGraph.at(codNodes.at(codNode)).at(i) == 1){
				Ptr<NetDevice> outIf = ncDevices.at(codNodes.at(codNode)).at(i).Get(0);
				multicast.SetDefaultMulticastRoute (src, outIf);
				srcSocket -> Send(outPkt, 0);
			}
		}

		srcSocket -> Close();

		uint32_t nR = ipv4SR -> GetNMulticastRoutes();
		for(uint32_t i = 0; i < nR; i++){
			ipv4SR -> RemoveMulticastRoute(0);
		}
	}

}

std::vector<std::vector<std::vector<long int>>> destRxPkt;
std::vector<std::vector<int>> destRxFlags;
void DestSinkRxTrace(std::string context, Ptr<const Packet> pkt, const Address &addr){
	std::regex rgx("(\\d+)");
	std::smatch match;
	uint32_t destNode = 0; //destNode that made Callback
	if(std::regex_search(context, match, rgx)){
		destNode =  std::stoi(match[1]);                //destNode #
		destNode = getElemPosV(destNodes, destNode); //change to pos in destNodes
	}

	uint8_t pktC; //flag from pkt
	MyTag idTag;
	if(pkt -> PeekPacketTag(idTag)){
		pktC = idTag.GetSimpleValue();
	}

	//pktContent
	uint8_t* buffer = new uint8_t[pkt->GetSize()];
	pkt -> CopyData (buffer, pkt->GetSize());

	int destFlagPos = getElemPosV(destRxFlags.at(destNode), pktC);
	if(destFlagPos == (int)destRxFlags.at(destNode).size()){
		destRxFlags.at(destNode).push_back((int)pktC);
	}
	//save pkt content in one of the vectors of destBuffer
	destRxPkt.at(destNode).at(destFlagPos).push_back(atol((char*)buffer));

	bool checkPkt = true;
	//Checks if there's a stored pkt for each in IF
	for(uint32_t i = 0; i < destRxPkt.at(destNode).size(); i++){
		if(destRxPkt.at(destNode).at(i).size() == 0){ //no hay paquetes con flag i
			checkPkt = false;
		}
	}

	if(checkPkt){

		Ptr<Socket> srcSocket = Socket::CreateSocket(ncNodes.Get(destNodes.at(destNode)),
		 																						UdpSocketFactory::GetTypeId());
		srcSocket -> Connect(InetSocketAddress(multicastGroup, multicastPort));


		//if there's no A+B, send first packet of each buffer.
		Ptr<Node> src = ncNodes.Get(destNodes.at(destNode));
		Ipv4StaticRoutingHelper multicast;
		for(uint32_t i = 0; i < destRxPkt.at(destNode).size(); i++){
			std::ostringstream msgx;
			MyTag outTag;

			long int pktdest = destRxPkt.at(destNode).at(i).at(0);
			uint8_t tag = destRxFlags.at(destNode).at(i);
			if(tag == 0x3){ //works for minmaxflux = 2
				int i2 = 0;
				if(i == 0){
					i2 = 1;
				}
				long int pktdest2 = destRxPkt.at(destNode).at(i2).at(0);
				uint8_t tag2 = destRxFlags.at(destNode).at(i2);

				pktdest = pktdest ^ pktdest2;
				tag     = tag ^ tag2;
			}

			msgx << pktdest;
			outTag.SetSimpleValue(tag);
			Ptr<Packet> outPkt = Create<Packet>((uint8_t*) msgx.str().c_str(), pkt -> GetSize());
			outPkt -> AddPacketTag(outTag);

			//a Dest only has one outIf to the terminal attached
			Ptr<NetDevice> outIf = tDevices.at(destNode + 1).Get(0);
			multicast.SetDefaultMulticastRoute (src, outIf);
			srcSocket -> Send(outPkt, 0);

		}

		//after sending, erase first pkt from each buffer
		for(uint32_t i = 0; i < destRxPkt.at(destNode).size(); i++){
			destRxPkt.at(destNode).at(i).erase(destRxPkt.at(destNode).at(i).begin());
		}


		srcSocket -> Close();

		Ptr<Ipv4> ipv4Src = src -> GetObject<Ipv4>();
		Ptr<Ipv4StaticRouting> ipv4SR = multicast.GetStaticRouting(ipv4Src);

		uint32_t nR = ipv4SR -> GetNMulticastRoutes();
		for(uint32_t i = 0; i < nR; i++){
			ipv4SR -> RemoveMulticastRoute(0);
		}
	}
}

std::vector<bool> termFirst;
std::vector<uint32_t> termAIndex;
void TerminalSinkRxTrace(std::string context, Ptr<const Packet> pkt, const Address &addr){
	std::regex rgx("(\\d+)");
	std::smatch match;
	uint32_t termNode = 0; //destNode that made Callback
	if(std::regex_search(context, match, rgx)){
		termNode =  std::stoi(match[1]);                //destNode #
		//id here is tNodes(node - ffgraph.size)
		termNode = termNode - ffGraph.size();
	}

	std::ofstream termLog;
	if(termFirst.at(termNode)){
		termLog.open(dirPath+"/terminal_" + std::to_string(termNode) +".plg", std::ofstream::trunc);
    termLog << "Paquetes Recibidos por Terminal #" << destNodes.at(termNode - 1) << ":" <<
																									std::endl << std::endl;
		termFirst.at(termNode) = false;
	}else{
		termLog.open(dirPath+"/terminal_" + std::to_string(termNode) +".plg", std::ofstream::app);
	}

	//pktContent
	uint8_t* buffer = new uint8_t[pkt->GetSize()];
	pkt -> CopyData (buffer, pkt->GetSize());
	double timeNow = Simulator::Now().GetSeconds();
	termLog << "at time " << timeNow << " | pkt: ";
	termLog << (atol((char*)buffer)) << std::endl;
	termLog.close();

	analysis.at(termAIndex.at(termNode - 1)).at(1 + termNode) = std::to_string(timeNow);
	termAIndex.at(termNode - 1) = termAIndex.at(termNode - 1) + 1;
}

void simulationAnalysis(std::string dirPath, uint32_t pktNPair){
	std::vector<std::vector<double>> pktTravelTime(analysis.size(),
									std::vector<double>(destNodes.size()));
	for(uint32_t i = 0; i < 2; i++){
		double startTime = std::stod(analysis.at(i).at(1));
		for(uint32_t j = 2; j < analysis.at(i).size(); j++){
			pktTravelTime.at(i).at(j - 2) = std::stod(analysis.at(i).at(j))  - startTime;
		}
	}

	std::ofstream alsLog;
	alsLog.open(dirPath+"/statistics.plg", std::ofstream::trunc);
  alsLog << "Analisis de envío de paquetes (Paquetes enviados: " << pktNPair * 2 <<
																								"):" << std::endl << std::endl;

	alsLog << "Tiempo de llegada promedio por destino para paquete tipo 01:" << std::endl;
	for(uint32_t i = 0; i <pktTravelTime.at(0).size(); i++){
		alsLog << "Destino #" << i << "(Nodo " << destNodes.at(i) <<
																"): " << pktTravelTime.at(0).at(i) << std::endl;
	}
	alsLog << std::endl;

	alsLog << "Tiempos de llegada promedio por destino para paquete tipo 10:" << std::endl;
	for(uint32_t i = 0; i <pktTravelTime.at(1).size(); i++){
		alsLog << "Destino #" << i << "(Nodo " << destNodes.at(i) <<
																"): " << pktTravelTime.at(1).at(i) << std::endl;
	}
	alsLog << std::endl;

	alsLog << "Tiempo total por destino:" << std::endl;
	for(uint32_t i = 2; i < analysis.back().size(); i++){
		alsLog << "Destino #" << i - 2 << "(Nodo " << destNodes.at(i - 2) <<
																"): " << std::stod(analysis.back().at(i)) - 1.0 << std::endl;
	}
	alsLog << std::endl;


	alsLog.close();

}


std::vector<int> getMatrixToGraphLevels(std::vector<std::vector<int>> ffGraph){
	std::vector<int> levels (ffGraph.size(), 0);
	levels.at(0) = 1;
	std::vector<int> searchQ;
	searchQ.push_back(0);
	while(!searchQ.empty()){
		int node = searchQ.at(0);
		searchQ.erase(searchQ.begin());

		for(uint32_t i = 0; i < ffGraph.size(); i++){
			if(ffGraph.at(node).at(i) == 1){
				levels.at(i) = levels.at(node) + 1;

				if(getElemPosV(searchQ, i) == (int)searchQ.size()){
					searchQ.push_back(i);
				}
			}
		}
	}
	return levels;
}


int main (int argc, char *argv[]){

	std::cout << "NC-CheckPoint Start" << std::endl;
  CommandLine cmd;
  std::string ffInput;
	uint32_t pktNPair;
  cmd.AddValue ("ffInput", "Name of Input File", ffInput);
	cmd.AddValue ("pktNPair", "Number of Pkt Pairs", pktNPair);
  cmd.Parse (argc,argv);

	std::string line;
	std::ifstream Capacity("./scratch/network_coding_pf/NS3Inputs/"+ffInput+".ffm");


	if(Capacity.is_open()){
		std::getline(Capacity, line);
		std::stringstream ss(line);
		std::string item;
		while(std::getline(ss, item, ' ')){
			destNodes.push_back(stoi(item));
		}

		getline(Capacity, line);
		std::stringstream ss2(line);
		while(std::getline(ss2, item, ' ')){
			codNodes.push_back(stoi(item));
		}

		std::getline(Capacity, line);
		std::stringstream ss3(line);
		while(std::getline(ss3, item, ' ')){
			srcPackages.push_back(stoi(item));
		}

		while(std::getline(Capacity, line)){
			std::vector<int> col;
			std::stringstream ss4(line);
			while(std::getline(ss4, item, ' ')){
				col.push_back(stoi(item));
			}
			ffGraph.push_back(col);
		}
		Capacity.close();
	}else{
		std::cout << "Unable to open file\n";
	}

	std::cout << "NC-CheckPoint Input read" << std::endl;

  LogComponentEnable ("UdpEchoClientRApplication", LOG_LEVEL_INFO);


  ncNodes.Create (ffGraph.size());

  tNodes.Create(1); //Source
  tNodes.Create(destNodes.size()); //Destinos

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

	ncDevices = std::vector<std::vector<NetDeviceContainer>>(ffGraph.size(),
	 				std::vector<NetDeviceContainer>(ffGraph.size(), NetDeviceContainer()));
	tDevices = std::vector<NetDeviceContainer>(tNodes.GetN(), NetDeviceContainer());

  //conexión terminal 0 -> Source
  NodeContainer temp = NodeContainer();
  temp.Add(tNodes.Get(0));
  temp.Add(ncNodes.Get(0));
  tDevices.at(0) = pointToPoint.Install(temp);

  //conexiones del grafo nodo i -> nodo j
  for(uint32_t i = 0; i < ffGraph.size(); i++){
    for(uint32_t j = 0; j < ffGraph.size(); j++){
      if(ffGraph.at(i).at(j) == 1){
        temp = NodeContainer();
        temp.Add(ncNodes.Get(i));
        temp.Add(ncNodes.Get(j));
        ncDevices.at(i).at(j) = pointToPoint.Install(temp);
      }
    }
  }

  //conexiones destino i -> terminal i
  for(uint32_t i = 1; i < tNodes.GetN(); i++){
    temp = NodeContainer();
    temp.Add(ncNodes.Get(destNodes.at(i - 1))); //ncNodes.Get ( #dest )
    temp.Add(tNodes.Get(i));
    tDevices.at(i) = pointToPoint.Install(temp);
  }

	std::cout << "NC-CheckPoint Nodes and Devices Created" << std::endl;

  InternetStackHelper stack;
  stack.InstallAll();

  Ipv4AddressHelper address;
  int a = 1;
  int b = 0;
  int c = 0;
  int d = 0;


	ncInterfaces = std::vector<std::vector<Ipv4InterfaceContainer>>(ffGraph.size(),
	             std::vector<Ipv4InterfaceContainer>(ffGraph.size(), Ipv4InterfaceContainer()));
	tInterfaces = std::vector<Ipv4InterfaceContainer>(ffGraph.size(), Ipv4InterfaceContainer());
	//IPs para las redes en el grafo
  for(uint32_t i = 0; i < ffGraph.size(); i++){
    for(uint32_t j = 0; j < ffGraph.size(); j++){
      if(ffGraph.at(i).at(j) == 1){
        std::string addr = "" + std::to_string(a) + "." + std::to_string(b) + "." + std::to_string(c) + "." + std::to_string(d);
        address.SetBase(Ipv4Address(addr.c_str()),"255.255.255.252");
        ncInterfaces.at(i).at(j) = address.Assign (ncDevices.at(i).at(j));

        //todas las conexiones p2p se hicieron en redes con subnetting a 2 hosts. mascara /30
        d = d + 4;
        if(d == 256){
          d = 0;
          c = c + 1;
        }
        if(c == 256){
          c = 0;
          b = b + 1;
        }
        if(b == 256){
          b = 0;
          a = a + 1;
        }
        if(a == 256){
          return 0; //no hay suficientes redes.
        }
      }
    }
  }

  //IPs para las redes entre terminales y nodos source/dest
  //tInterface(0) es el source multicast.
  for(uint32_t i = 0; i < tNodes.GetN(); i++){
    std::string addr = "" + std::to_string(a) + "." + std::to_string(b) + "." + std::to_string(c) + "." + std::to_string(d);
    address.SetBase(addr.c_str(),"255.255.255.252");
    tInterfaces.at(i) = address.Assign (tDevices.at(i));

    d = d + 4;
    if(d == 256){
      d = 0;
      c = c + 1;
    }
    if(c == 256){
      c = 0;
      b = b + 1;
    }
    if(b == 256){
      b = 0;
      a = a + 1;
    }
    if(a == 256){
      return 0; //no hay suficientes redes.
    }
  }

	std::cout << "NC-CheckPoint Ipv4IF SET" << std::endl;


  multicastSource = (tInterfaces.at(0).GetAddress(0));


  Ipv4StaticRoutingHelper multicast;

  //Set up a Default Multicast Route to terminal 0.
  Ptr<Node> sender = tNodes.Get(0);
  Ptr<NetDevice> senderIf = tDevices.at(0).Get(0);
  multicast.SetDefaultMulticastRoute (sender, senderIf);

	std::cout << "NC-CheckPoint multiSource / T0 -> N0 Set" << std::endl;

  //Configure a (static) multicast route on all nodes
  std::vector<int> multiQ;
  //multiQ.push_back(0);
  std::vector<int> lastNQ;
  //lastNQ.push_back(-1);//last revised node, init at -1 because first last is the terminal 0

	for(uint32_t i = 0; i < ffGraph.size(); i++){
		if(ffGraph.at(0).at(i) == 1){
			multiQ.push_back(i);
			lastNQ.push_back(0);
		}
	}

  while(!multiQ.empty()){
    int node = multiQ.at(0);
    multiQ.erase(multiQ.begin());
    int lastNode = lastNQ.at(0);
    lastNQ.erase(lastNQ.begin());
		uint32_t isDes = getElemPosV(destNodes, node);
		uint32_t isCod = getElemPosV(codNodes,  node);

    Ptr<Node> multicastRouter = ncNodes.Get (node);  // The node in question
		Ptr<NetDevice> inputIf;  // The input NetDevice
    if(lastNode == -1){ // if it's node 0, interface is T0 -> N0
      inputIf = tDevices.at(0).Get(1);
    }else{ //else is lastN -> Ni
      inputIf = ncDevices.at(lastNode).at(node).Get(1);
    }
    NetDeviceContainer outputDevices;  // A container of output NetDevices
    for(uint32_t i = 0; i < ffGraph.size(); i++){
      if(ffGraph.at(node).at(i) == 1){
        multiQ.push_back(i);
        lastNQ.push_back(node);
        outputDevices.Add (ncDevices.at(node).at(i).Get(0)); //the output NetDevice
      }
    }
		//Ignores CodNodes and DestNodes
		if(isDes == destNodes.size() && isCod == codNodes.size()){
	    multicast.AddMulticastRoute (multicastRouter, multicastSource,
	                                 multicastGroup, inputIf, outputDevices);
	  }

  }

	std::cout << "NC-CheckPoint regNodes multicast routing Set" << std::endl;

/*
	//Routing from destNodes to the terminals attached to each one
  //Routing from destNodes is actually default, all trafic will go through if to Ti
  for(uint32_t i = 0; i < destNodes.size(); i++){
    int dest = destNodes.at(i);
    Ptr<Node> destNodeR = ncNodes.Get (dest); //Dest Node
    Ptr<NetDevice> destNodeRIF = tDevices.at(i + 1).Get(0); //output IF from DESTi to Ti;
    multicast.SetDefaultMulticastRoute (destNodeR, destNodeRIF);
  }

  //Routing from codNodes is default?
  //don't think so, cod nodes can have multiple out interfaces.
  //actually should be Default but, All trafic will go through ALL OutputIF
  //I think if sink consumes then generate packets with socket.
	for(uint32_t i = 0; i < codNodes.size(); i++){
    int dest = codNodes.at(i);
    Ptr<Node> codNodeR = ncNodes.Get (dest); //Dest Node
		for(uint32_t j = 0; j < ffGraph.size(); j++){
			if(ffGraph.at(dest).at(j) == 1){
				Ptr<NetDevice> codNodeRIF = ncDevices.at(dest).at(j).Get(0); //output IF from CODi to Ni;
		    multicast.SetDefaultMulticastRoute (codNodeR, codNodeRIF);
			}
		}
  }

	std::cout << "NC-CheckPoint dest/cod Nodes multicast routing Set" << std::endl;
*/

  UdpEchoClientRHelper echoClient(multicastGroup, multicastPort, true);;
  echoClient.SetAttribute ("MaxPackets", UintegerValue (pktNPair * 2));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (0.0005)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (128));

  ApplicationContainer clientApp = echoClient.Install (tNodes.Get(0));
  clientApp.Start(Seconds (1.0));
  clientApp.Stop (Seconds (10.0));

	//Callback on UdpEchoClient to set flags before send on T0
	Config::ConnectWithoutContext ("/NodeList/" + std::to_string(tNodes.Get(0)->GetId()) +
                                 "/ApplicationList/*/$ns3::UdpEchoClientR/Tx", MakeCallback (&SourceUdpTxTrace));

	PacketSinkHelper sink ("ns3::UdpSocketFactory",
    InetSocketAddress (Ipv4Address::GetAny (), multicastPort));
	//PacketSinks are installed in Source, Cod and Dest Routers for coding and decoding
	//PacketSinks are installed in terminals to verify.

  //Sink for Source Router.
  ApplicationContainer sinkS = sink.Install (ncNodes.Get(0));
  sinkS.Start (Seconds (1.0));
  sinkS.Stop (Seconds (10.0));
  Config::ConnectWithoutContext ("/NodeList/" + std::to_string(ncNodes.Get(0)->GetId()) +
                                 "/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback (&SourceSinkRxTrace));

  //Sink for Cod Routers.
	codRxPkt = std::vector<std::vector<std::vector<long int>>>(codNodes.size(),
	 							std::vector<std::vector<long int>>(2, std::vector<long int>()));
	codRxFlags = std::vector<std::vector<int>>(codNodes.size(), std::vector<int>());
  for(uint32_t i = 0; i < codNodes.size(); i++){
    ApplicationContainer sinkC = sink.Install (ncNodes.Get(codNodes.at(i)));
    sinkC.Start (Seconds (1.0));
    sinkC.Stop (Seconds (10.0));
    Config::Connect ("/NodeList/" + std::to_string(ncNodes.Get(codNodes.at(i))->GetId()) +
                                   "/ApplicationList/*/$ns3::PacketSink/Rx",
                                  MakeCallback (&CodSinkRxTrace));
  }

  //Sink for Dest Routers.
	destRxPkt = std::vector<std::vector<std::vector<long int>>>(destNodes.size(),
	 							std::vector<std::vector<long int>>(2, std::vector<long int>()));
	destRxFlags = std::vector<std::vector<int>>(destNodes.size(), std::vector<int>());
	codFirst = std::vector<bool>(codNodes.size(), true);
  for(uint32_t  i = 0; i < destNodes.size(); i++){
    ApplicationContainer sinkD = sink.Install (ncNodes.Get(destNodes.at(i)));
    sinkD.Start (Seconds (1.0));
    sinkD.Stop (Seconds (10.0));
    Config::Connect ("/NodeList/" + std::to_string(ncNodes.Get(destNodes.at(i))->GetId()) +
                                   "/ApplicationList/*/$ns3::PacketSink/Rx",
                                  MakeCallback (&DestSinkRxTrace));
  }

  //Sink for Terminals.
	termFirst  = std::vector<bool>(tNodes.GetN(), true);
	termAIndex = std::vector<uint32_t>(tNodes.GetN(), 0);
  for(uint32_t i = 1; i < tNodes.GetN(); i++){
    ApplicationContainer sinkT = sink.Install (tNodes.Get(i));
    sinkT.Start (Seconds (1.0));
    sinkT.Stop (Seconds (10.0));
    Config::Connect ("/NodeList/" + std::to_string(tNodes.Get(i)->GetId()) +
                                   "/ApplicationList/*/$ns3::PacketSink/Rx",
                                  MakeCallback (&TerminalSinkRxTrace));
  }

  std::cout << "NC-CheckPoint echoClient, PacketSinks Set" << std::endl;

	std::vector<int> levels = getMatrixToGraphLevels(ffGraph);
	std::vector<int> sizes;
	for(uint32_t i = 0; i < levels.size(); i++){
		if(levels.at(i) > (int)sizes.size()){
			sizes.resize(levels.at(i));
		}
		if(levels.at(i) != 0){
			sizes.at(levels.at(i) - 1) = sizes.at(levels.at(i) - 1) + 1;
		}
	}

	std::vector<int> lCount(sizes.size(), 0);
	lCount.push_back(1);
	AnimationInterface::SetConstantPosition(tNodes.Get(0), 0, 0);
	for(uint32_t i = 0; i < levels.size(); i++){
		int l = levels.at(i);
		if(l != 0){
			int isDes = getElemPosV(destNodes, i);
			int x = 0;
			if(sizes.at(l - 1) % 2 != 0){
				int init = sizes.at(l - 1) / 2;
				x = -50 * init + lCount.at(l - 1) * 50;
			}else{
				int init = sizes.at(l - 1) / 2;
				int t = lCount.at(l - 1) <= init ? -30 - 50 * (init - 1) : 30;
				int c = lCount.at(l - 1) <= init ? lCount.at(l - 1) : lCount.at(l - 1) - init;
				x = t + 50 * c;
			}
			AnimationInterface::SetConstantPosition(ncNodes.Get(i), x, levels.at(i) * 60);
			lCount.at(l - 1) = lCount.at(l - 1) + 1;

			if(isDes != (int)destNodes.size()){
				AnimationInterface::SetConstantPosition(tNodes.Get(lCount.at(lCount.size() - 1)), x - 20,
				 												 levels.at(i) * 60 + 20);
				lCount.at(lCount.size() - 1) = lCount.at(lCount.size() - 1) + 1;
			}
		}
	}
	AnimationInterface  anim("NCAnimation.xml");
	uint32_t nodeRes     = anim.AddResource(
		"/home/cisco/workspace/ns-3-allinone/ns-3-dev/scratch/network_coding_pf/Media/router-blue.png");
	uint32_t srcNodeRes     = anim.AddResource(
		"/home/cisco/workspace/ns-3-allinone/ns-3-dev/scratch/network_coding_pf/Media/router-green.png");
	uint32_t destNodeRes = anim.AddResource(
		"/home/cisco/workspace/ns-3-allinone/ns-3-dev/scratch/network_coding_pf/Media/router-red.png");
  uint32_t codNodeRes  = anim.AddResource(
		"/home/cisco/workspace/ns-3-allinone/ns-3-dev/scratch/network_coding_pf/Media/router-purple.png");
	uint32_t terminalRes = anim.AddResource(
		"/home/cisco/workspace/ns-3-allinone/ns-3-dev/scratch/network_coding_pf/Media/terminal.png");


	for(uint32_t i = 0; i < ncNodes.GetN(); i++){
		anim.UpdateNodeImage(ncNodes.Get(i) -> GetId(), nodeRes);
		if(levels.at(i) != 0){
			anim.UpdateNodeSize(ncNodes.Get(i) -> GetId(), 20, 20);
		}else{
			anim.UpdateNodeSize(ncNodes.Get(i) -> GetId(), 0, 0);
		}
	}

	//Update Src Router Image.
	anim.UpdateNodeImage(ncNodes.Get(0) -> GetId(), srcNodeRes);

	//Update Cod Routers Image.
  for(uint32_t i = 0; i < codNodes.size(); i++){
    anim.UpdateNodeImage(ncNodes.Get(codNodes.at(i)) -> GetId(), codNodeRes);
  }

  //Update for Dest Routers Image.
  for(uint32_t  i = 0; i < destNodes.size(); i++){
    anim.UpdateNodeImage(ncNodes.Get(destNodes.at(i)) -> GetId(), destNodeRes);
  }

  //Sink for Terminals.
  for(uint32_t i = 0; i < tNodes.GetN(); i++){
    anim.UpdateNodeImage(tNodes.Get(i) -> GetId(), terminalRes);
		anim.UpdateNodeSize(tNodes.Get(i) -> GetId(), 20, 20);
		//animation.UpdateNodeColor(t0r0.Get(0), 122, 186, 122);
  }

	std::cout << "NC-CheckPoint animation Set" << std::endl;

	dirPath = "./scratch/network_coding_pf/NS3Outputs/"+ffInput;
	mkdir(dirPath.c_str(), 0777);

	Simulator::ScheduleDestroy(&simulationAnalysis, dirPath, pktNPair);
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
 }
