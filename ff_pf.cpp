﻿#include <iostream>
#include <vector>
#include <tuple>
#include <limits>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
using namespace std;

string displayMatrix(vector<vector<int>> matrix, int n, int m){
	string mat = "";
	for(int i = 0; i < n; i++){
		mat += to_string(matrix.at(i).at(0));
		for(int j = 1; j < m; j++){
			mat += " " + to_string(matrix.at(i).at(j));
		}
		mat += "\n";
	}
	mat += "\n";
	return mat;
}

vector<int> pathAumentado (int inicio, int fin, vector<vector <int>> flujoActual,
				vector<vector <int>> capacidad,	int nNodos){

	vector<int> color (nNodos,0);
	vector<int> q;
	vector<int> pathAum;
	vector<int>::iterator it;

	//Encolamiento
	it = q.insert(q.begin(),inicio);
	color.at(inicio) = 1; //Gris/1 nodo de inicio?
	vector<int> pred (nNodos,0);

	while(!q.empty()){
		int u = q.back();
		q.pop_back();
		color.at(u) = 2; //Negro/2
		//Final de densencolamiento

		for (int v=0; v<nNodos; v++){
			if(color.at(v) == 0 && capacidad.at(u).at(v) > flujoActual.at(u).at(v)){
				//Encole (v,q)
				it = q.insert(q.begin(),v);
				color.at(v) = 1;
				//Final de densencolamiento
				pred.at(v) = u;
			}
		}
	}
	if(color.at(fin) == 2){ //Fin es accesible
		int temp = fin;
		while(pred.at(temp) != inicio){
			it = pathAum.insert(pathAum.begin(),pred.at(temp));
			temp = pred.at(temp);
		}
		it = pathAum.insert(pathAum.begin(),inicio);
		it = pathAum.insert(pathAum.end(),fin);
	}
	else{
		pathAum.clear();
	}
	return pathAum;
}

tuple<int,vector<vector<int>>> flujoMax(int inicio, int fin,
																		 vector<vector<int>> capacidad, int nNodos){

	vector<vector<int>> flujoActual (nNodos, vector<int>(nNodos,0));
	int flujoM = 0;
	vector <int> pathAum = pathAumentado(inicio, fin, flujoActual, capacidad, nNodos);

	while (!pathAum.empty()) {
		//si existe un camino aumentado actualize el flujo actual
		int incremento = std::numeric_limits<int>::max();

		for (int i = 0; i < pathAum.size() - 1; i++) {
			int a = capacidad.at(pathAum.at(i)).at(pathAum.at(i+1)) -
							flujoActual.at(pathAum.at(i)).at(pathAum.at(i+1));
			incremento=min(incremento, a);
		}
		//Incremento en el flujo actual
		for (int i = 0; i < pathAum.size() - 1; i++) {
			flujoActual.at(pathAum.at(i)).at(pathAum.at(i + 1)) = flujoActual.at(pathAum.at(i)).at(pathAum.at(i + 1)) + incremento;
			flujoActual.at(pathAum.at(i + 1)).at(pathAum.at(i)) = flujoActual.at(pathAum.at(i + 1)).at(pathAum.at(i)) - incremento;
		}
		flujoM = flujoM + incremento;
		//Busqueda de un nuevo path aumentado
		pathAum = pathAumentado(inicio, fin, flujoActual, capacidad, nNodos);
	}
	for (int i = 0; i < nNodos; i++) {
		for (int j = 0; j < nNodos; j++) {
			if(flujoActual.at(i).at(j) < 0){
				flujoActual.at(i).at(j) = 0;
			}
		}
	}

	return make_tuple(flujoM, flujoActual);
}

vector<int> nodos_com(vector<vector<int>> graf_red, int dest, int nNodos){
	vector<int> n_com;
	for(int i = 0; i < nNodos; i++){
		if(i != dest){
			int padres = 0;
			for(int j = 0; j < nNodos; j++){
				if(graf_red.at(j).at(i) == 1){
					padres ++;
				}
			}
			if(padres > 1){
				n_com.push_back(i);
			}
		}
	}
	return n_com;
}

vector<vector<int>> allPathsToDest(vector<vector<int>> optC, int origen, int dest){
	//init vector with 1 path, origen.
	vector<vector<int>> searchQ(1, vector<int>(1, origen));
	vector<vector<int>> allPaths;
	while(!searchQ.empty()){
		vector<int> pathT = searchQ.at(0);
		searchQ.erase(searchQ.begin());

		//todos los hijos del ultimo nodo de pathT.
		vector<int> desc;
		for(int i = 0; i < optC.size(); i++){
			if(optC.at(pathT.at(pathT.size() - 1)).at(i) == 1){
				desc.push_back(i);
			}
		}
		//agrega los descendientes a pathT y agrega a la cola
		for(int i = 0; i < desc.size(); i++){
			vector<int> copy = pathT;
			copy.push_back(desc.at(i));
			if(desc.at(i) == dest){
				allPaths.push_back(copy);
			}
			searchQ.push_back(copy);
		}
	}

	return allPaths;
}

vector<int> intersectV(vector<int> A, vector<int> B){
	vector<int> inters;
	for(int i = 0; i < B.size(); i++){
		if(find(A.begin(), A.end(), B.at(i)) != A.end()){
			inters.push_back(B.at(i));
		}
	}
	return inters;
}

vector<int> intersectAll(vector<vector<int>> V){
	int i = 1;
	vector<int> inters = V.at(0);
	while(i < V.size() && !inters.empty()){
		inters = intersectV(inters, V.at(i));
		i++;
	}
	return inters;
}

vector<int> getNodeParents(vector<vector<int>> optC, int node){
	vector<int> padres;
	for(int i = 0; i < optC.size(); i++){
		if(optC.at(i).at(node) == 1){
			padres.push_back(i);
		}
	}
	return padres;
}

vector<int> getNodeDest(vector<vector<int>> optC, vector<int> destinos, int cod){
	vector<int> dest;
	vector<int> searchQ;
	searchQ.push_back(cod);
	while(!searchQ.empty()){
		int nodeT = searchQ.at(0);
		searchQ.erase(searchQ.begin());

		if((find(destinos.begin(), destinos.end(), nodeT) != destinos.end())){
			if((find(dest.begin(), dest.end(), nodeT) == dest.end())){
				dest.push_back(nodeT);
			}
		}

		for(int i = 0; i < optC.size(); i++){
			if(optC.at(nodeT).at(i) == 1){
				searchQ.push_back(i);
			}
		}
	}
	return dest;
}

int trimCodNode(vector<vector<int>> optC, vector<int> destinos, int origen, int padre, int cod){
	int cut = -1;
	optC.at(padre).at(cod) = 0;
	vector<int> codDests = getNodeDest(optC, destinos, cod);
	bool trimable = true;
	int i = 0;

	while(i < codDests.size() && trimable){
		vector<vector<int>> rutas = allPathsToDest(optC, origen, codDests.at(i));

		int intC = (intersectAll(rutas)).size();
		// interseccion siemptre tendra origen y dest.
		if(intC > 2){
			trimable = false;
		}
		i++;
	}
	if(trimable){
		cut = padre * optC.size() + cod; //id del corte
	}

	return cut;
}

int getElemPosV(vector<int> V, int elem){
	return (find(V.begin(), V.end(), elem)) - V.begin();
}


vector<vector<int>> ffGraphP;
vector<int> destNodesP;
vector<int> codNodesP;
vector<vector<int>>   distrib; //matriz principal
vector<vector<bool>>  valid;   //matriz de validez
vector<int> 					pathQ;   //cola de revisión
vector<vector<int>> packages;  //distribución de paquetes
bool error = false;            //control de validez de la solución
int nonCodNodes;
vector<int> validPackages = {01, 10, 11};

vector<int> exploreSNodes(int origen){
	vector<int> exploreQ;
	vector<int> endPoints;
	exploreQ.push_back(origen);

	while(!exploreQ.empty()){
		int node = exploreQ.at(0);
		exploreQ.erase(exploreQ.begin());
		int isDes = getElemPosV(destNodesP, node);
		int isCod = getElemPosV(codNodesP, node);
		if(node != origen && (isDes != destNodesP.size() || isCod != codNodesP.size())){
			endPoints.push_back(node);
		}else{
			for(int i = 0; i < ffGraphP.size(); i++){
				if(ffGraphP.at(node).at(i) == 1){
					exploreQ.push_back(i);
				}
			}
		}
	}
	return endPoints;
}

vector<int> getValidColumn(int col){
	vector<int> validC;
	for(int i = 0; i < distrib.at(col).size(); i++){
		if(!valid.at(col).at(i)){
			validC.push_back(distrib.at(col).at(i));
		}
	}
	return validC;
}

vector<int> unionV(vector<int> A, vector<int> B){
		for(int i = 0; i < B.size(); i++){
			if(getElemPosV(A, B.at(i)) == A.size()){
				A.push_back(B.at(i));
			}
		}
		return A;
}

vector<int> getOppositePackagesV(vector<int> p){
	vector<int> opp;
	vector<int> copy = validPackages;
	for(int i = 0; i < p.size(); i++){
		int tPos = getElemPosV(copy, p.at(i));
		copy.erase(copy.begin() + tPos);
	}
	return copy;
}

//selectOnePackage::regresa un vector con un paquete diferente a 11
vector<int> selectOnePackage(vector<int> pack){
	int tPos = getElemPosV(pack, 11);
	if(tPos != pack.size()){
		pack.erase(pack.begin() + tPos);
	}

	return vector<int>(1, pack.at(0));
}

void checkCodCol(int colC);

void setCodCol(int colC, vector<int> p){
	if(packages.at(colC).empty()){
		packages.at(colC) = p;
	}else{
		packages.at(colC) = intersectV(packages.at(colC), p);
	}
	if(packages.at(colC).empty()){
		error = true;
	}
	if(packages.at(colC).size() == 1){
		checkCodCol(colC);
	}
}

void intersectColumns(int col){
	vector<int> validNodes = getValidColumn(col);
	while(!validNodes.empty() && !error){
		int maxInt  = 0;        //# de intersecciones entre las 2 columnas
		int colI  = -1;       //indice de la columna que mas intersecta
		vector<int> optInter;   //los nodos intersectados

		//busca la columna opuesta optima a col. y el vector con los elementos intersectados.
		for(int i = 0; i < distrib.size(); i++){
			if(i != col){
				vector<int> cTemp = getValidColumn(i);
				vector<int> inter = intersectV(validNodes, cTemp);

				if(inter.size() > maxInt){
					maxInt  = inter.size();
					colI  = i;
					optInter = inter;
				}
			}
		}

		//la columna I se marca con los opuestos de col.
		vector<int> p = getOppositePackagesV(packages.at(col));
		if(packages.at(colI).empty()){
			packages.at(colI) = p;
		}else{
			packages.at(colI) = intersectV(packages.at(colI), p);
		}
		if(packages.at(colI).empty()){
			error = true;
		}

		//marca las casillas que se solucionaron con la intersección.
		for(int i = 0; i < optInter.size(); i++){
			int rNum = getElemPosV(distrib.at(col), optInter.at(i));
			valid.at(col).at(rNum) = true;
			rNum = getElemPosV(distrib.at(colI), optInter.at(i));
			valid.at(colI).at(rNum) = true;
		}

		//si colI es cod verificar
		if(colI >= nonCodNodes){
			if(packages.at(colI).size() == 1){
				checkCodCol(colI);
			}
		}else{
			//la columna opuesta se agrega a la cola para ser revisada.
			pathQ.push_back(colI);
		}

		validNodes = getValidColumn(col);
	}
}

void checkCodCol(int colC){
	for(int i = 0; i < distrib.size(); i++){
		int f = getElemPosV(distrib.at(i), codNodesP.at(colC - nonCodNodes));
		if(f != distrib.at(i).size()){
			vector<int> p = getOppositePackagesV(packages.at(colC));
			if(packages.at(f).empty()){
				packages.at(f) = p;
			}else{
				packages.at(f) = intersectV(packages.at(f), p);
			}
			if(packages.at(f).empty()){
				error = true;
			}
		}
	}

	for(int i = 0; i < distrib.at(colC).size(); i++){
		int codPos = getElemPosV(codNodesP, distrib.at(colC).at(i));
		if(codPos != codNodesP.size()){
			codPos += nonCodNodes;
			setCodCol(codPos, getOppositePackagesV(packages.at(colC)));
		}
	}
	intersectColumns(colC);
}

vector<vector<int>> checkPackages(vector<vector<int>> ffGraph, vector<int> destNodes, vector<int> codNodes){
	ffGraphP = ffGraph;
	destNodesP = destNodes;
	codNodesP = codNodes;
	distrib = vector<vector<int>>();
	valid = vector<vector<bool>>();
	pathQ = vector<int>();
	error = false;


	//agregamos los S->N
	for(int i = 0; i < ffGraphP.size(); i++){
		if(ffGraphP.at(0).at(i) == 1){
			//explora descendentemente, se detiene en destino o codificador
			vector<int> temp = exploreSNodes(i);
			distrib.push_back(temp);
			valid.push_back(vector<bool>(temp.size(), false));
		}
	}

	//agregamos los codificadores
	nonCodNodes = distrib.size(); // separación entre nodos origen | codificadores
	for(int i = 0; i < codNodesP.size(); i++){
		vector<int> temp = exploreSNodes(codNodesP.at(i));
		distrib.push_back(temp);
		valid.push_back(vector<bool>(temp.size(), false));
	}

	packages = vector<vector<int>>(distrib.size(), vector<int>()); //paquetes se inicializa con un tamaño = distrib
	pathQ.push_back(0); //primera columna a revisar es la #1
	while(!pathQ.empty() && !error){
		int cNum = pathQ.at(0);
		pathQ.erase(pathQ.begin());
		vector<int> cNodes = getValidColumn(cNum);

		//Se fija un paquete en la columna en revisión
		if(packages.at(cNum).empty()){
			packages.at(cNum).push_back(01); //si no esta marcada se marca con 01
		}else{
			if(packages.at(cNum).size() > 1){
				packages.at(cNum) = selectOnePackage(packages.at(cNum)); //en flujo 2, si tiene 2 posibles escoge el simple
			}
		}

		//para cada codificador en cNum, marca con los opuestos
		for(int i = 0; i < distrib.at(cNum).size(); i++){
			int codPos = getElemPosV(codNodesP, distrib.at(cNum).at(i));
			if(codPos != codNodesP.size()){
				codPos += nonCodNodes;
				setCodCol(codPos, getOppositePackagesV(packages.at(cNum)));
			}
		}

		intersectColumns(cNum);

		if(pathQ.empty()){
			int vi = 0;
			while (vi < valid.size()){
				int t = (getValidColumn(vi)).size();
				if(t > 0){
					pathQ.push_back(vi);
					vi = valid.size();
				}
				vi++;
			}
		}
	}

	return packages;
}

int getState(vector<vector<int>> packages, int nonCodNodes){
	int state = 1;
	int i = 0;
	while(i < packages.size() && state != 0){
		if(packages.at(i).empty()){
			state = 0;
		}else{
			if(packages.at(i).at(0) == 11 && i < nonCodNodes){
				state = 2;
			}
		}
		i++;
	}
	return state;
}

vector<vector<int>> ffMulticast(int nNodos, vector<int> destinos, vector<vector<int>> capacidad, string fileName){
	ofstream Result("Results/"+fileName+".txt", std::ofstream::trunc);
	if(Result.is_open()){
		Result << "Resultado ffMulticast\n\n";
		Result << "No. de nodos: " << nNodos << "\n";

		string line = "Destinos: - ";
		for (int i = 0; i < destinos.size(); i++){
			line += to_string(destinos.at(i)) + " - ";
		}
		Result << line << "\n\n";

		Result << "Matriz de Capacidad:\n";
		Result << displayMatrix(capacidad, nNodos, nNodos);
		Result << "\n";

		vector<vector<vector<int>>> flujosArcoDes; // vector 3D que tiene todos los grafos individuales
		vector<int> flujosMaxDes; //vector que contiene los flujos maximos por grafo-destino
		for (int i = 0; i < destinos.size(); i++){
			int flujoM;
			vector<vector<int>> flujoArco;
			//#flujoMax, recibe el grafo individual y el flujo maximo del destino i
			tie(flujoM, flujoArco) = flujoMax(0, destinos.at(i), capacidad, nNodos);
			flujosMaxDes.push_back(flujoM);
			Result << "Flujo Maximo hacia " << destinos.at(i) << " es: " << flujoM << "\n";
			flujosArcoDes.push_back(flujoArco);
			Result << "Flujo de Arco hacia " << destinos.at(i) << " es: " << "\n";
			Result << displayMatrix(flujoArco, nNodos, nNodos);
		}

		//Busca el Flujo Maximo Mínimo
		int flujoMaxMin = flujosMaxDes.at(0);
		for (int i = 1; i < flujosMaxDes.size(); i++){
			flujoMaxMin = ((flujoMaxMin > flujosMaxDes.at(i)) ? flujosMaxDes.at(i) : flujoMaxMin);
		}

		//Marca los Grafo-Destinos que tienen Flujo Máximo > FlujoMaxMin
		vector<int> fmMayP;
		vector<int> fmMayV;
		for (int i = 0; i < flujosMaxDes.size(); i++){
			if (flujosMaxDes.at(i) != flujoMaxMin){
				fmMayP.push_back(i);
				fmMayV.push_back(flujosMaxDes.at(i));
			}
		}

		Result << "Flujo mínimo: " << flujoMaxMin << "\n\n";
		if(fmMayP.empty()){
			Result << "Todos los Grafos tienen Flujo Máximo igual al Flujo Mínimo\nNo es necesario recortar";
		}else{
			line = "Los Grafos Inviduales con destinos: " + to_string(fmMayP.at(0));
			for(int i = 1; i < fmMayP.size(); i++){
				line += ", " + to_string(fmMayP.at(i));
			}
			line += "\nTienen Flujo Máximo mayor al Flujo Mínimo\nSe debe Recortar";
			Result << line;
		}
		Result << "\n\n";

		//Pasa por todos los grafos que necesitan ser recortados.
		for(int i = 0; i < fmMayP.size(); i++){
			vector<vector<int>> fl_nod_com = flujosArcoDes.at(fmMayP.at(i));
			Result << "Recortando Grafo con Destino " + to_string(destinos.at(fmMayP.at(i))) + "\n";
			Result << "Se deben recortar " + to_string(fmMayV.at(i) - flujoMaxMin) + " camino(s)" + "\n";

			for(int j = 0; j < fmMayV.at(i) - flujoMaxMin; j++){
				vector<int> n_com = nodos_com(fl_nod_com, destinos.at(fmMayP.at(i)), nNodos);
				vector<vector<int>> rutas = allPathsToDest(fl_nod_com, 0, destinos.at(fmMayP.at(i)));

				//Corta la ruta mas larga (no hay nodos comunes)
				if(n_com.empty()){
					int vrut_max = rutas.at(0).size();;
					int pos_max = 0;
					for(int k = 1; k < rutas.size(); k++){
						if(rutas.at(k).size() > vrut_max){
							vrut_max = rutas.at(k).size();
							pos_max = k;
						}
					}
					int x = 0; //nodo origen
					for(int lc = 1; lc < rutas.at(pos_max).size(); lc++){
						int y = rutas.at(pos_max).at(lc);
						fl_nod_com.at(x).at(y) = 0;
						flujosArcoDes.at(fmMayP.at(i)).at(x).at(y) = 0;
						x = y;
					}

				}else{ //Corta la ruta con nodos mas comunes, mas larga
					int ncom = n_com.size();
					int max_ncom = 0;
					int pos_max_ncom = 0;
					int max_rut_size = 0;

					for(int r = 0; r < rutas.size(); r++){
						int num_inter = (intersectV(rutas.at(r),n_com)).size();

						if(num_inter == max_ncom){
							if(rutas.at(r).size() > max_rut_size){
								pos_max_ncom = r;
								max_rut_size = rutas.at(r).size();
							}
						}else{
							if(num_inter > max_ncom){
								max_ncom = num_inter;
								pos_max_ncom = r;
								max_rut_size = rutas.at(r).size();
							}
						}
					}

					//Elimina la ruta decidida del grafo individual i
					int x =rutas.at(pos_max_ncom).at(0);
					for(int c = 1; c < rutas.at(pos_max_ncom).size(); c++ ){
						int y = rutas.at(pos_max_ncom).at(c);
						fl_nod_com.at(x).at(y) = 0;
						flujosArcoDes.at(fmMayP.at(i)).at(x).at(y) = 0;
						x = y;
					}
					rutas.erase(rutas.begin() + pos_max_ncom);
				}
			}

			Result << "Grafo recortado: \n";
			Result << displayMatrix(flujosArcoDes.at(fmMayP.at(i)), nNodos, nNodos) + "\n";
		}

		//Reduce la matriz 3D flujosArcoDes a una matriz 2D fa_min
		vector<vector<int>> fa_min (nNodos, vector<int>(nNodos, 0));
		for(int i = 0; i < nNodos; i++){
			for(int j = 0; j < nNodos; j++){
				int a = 0;
				int k = 0;
				while (a < 1 && k < flujosArcoDes.size()){
					a = a + flujosArcoDes.at(k).at(i).at(j);
					k++;
				}
				if(a > 0){
					fa_min.at(i).at(j) = 1;
				}
			}
		}
		Result << "Grafo de Flujos Maximos Reducido: \n";
		Result << displayMatrix(fa_min, nNodos, nNodos);

		//Calculo de nodos de codificicación
		vector<int> nodos_cod;
		for(int i = 0; i < nNodos; i++){
			if((find(destinos.begin(), destinos.end(), i) == destinos.end())){
				int padres = 0;
				for(int j = 0; j < nNodos; j++){
					if(fa_min.at(j).at(i) == 1){
						padres++;
					}
				}
				if(padres > 1){
					nodos_cod.push_back(i);
				}
			}
		}

		if(nodos_cod.size() > 0){
			line = "Nodos de Codificación: \n- ";
			for (int i = 0; i < nodos_cod.size(); i++){
				line += to_string(nodos_cod.at(i)) + " - ";
			}
			Result << line + "\n";
		}else{
			Result << "No hay nodos de codificación\n";
		}
		Result << endl;


		vector<vector<int>> optC = fa_min;

		// para probar con un grafo directamente
		// vector<vector<int>> optC = capacidad;
		// nodos_cod = vector<int>();
		// nodos_cod.push_back(8);
		// nodos_cod.push_back(10);

		vector<vector<int>> optPackages;
		vector<int> optCortes;
		int maxTrim = -1;
		int optState = 0;
		int trimT = 0;
		bool opt = false;

		//probar si el grafo sin corte tiene solución válida
		//pState = Package State, 0:not solved, 1: solved, 2: solved with	Compound Packages
		vector<vector<int>> packages = checkPackages(optC, destinos, nodos_cod);
		int pState = getState(packages, packages.size() - nodos_cod.size());
		if(pState != 0){
			maxTrim = 0;
			optState = pState;
			optCortes = vector<int>();
			optPackages = packages;
		}

		if(nodos_cod.size() > 0){
			vector<int> parentQ; 				  //cola de padres a cortar.
			vector<int> trimQ;	 				  //cola de #nodos cortados.
			vector<vector<int>> cortesQ;  //cola de cortes realizados

			vector<int> padres = getNodeParents(optC, nodos_cod.at(trimT)); //para empezar
			parentQ.insert(parentQ.begin(), padres.begin(), padres.end());   //todos los padres del cod 1
			trimQ.insert(trimQ.begin(), padres.size(), trimT);               //primer Trim es 0
			cortesQ.insert(cortesQ.begin(), padres.size(), vector<int>());   //cortes son []

			while(!parentQ.empty() && !opt){
				//remueve primero de la cola.
				int padreT = parentQ.at(0);
				parentQ.erase(parentQ.begin());
				trimT = trimQ.at(0);
				trimQ.erase(trimQ.begin());
				vector<int> corteT = cortesQ.at(0);
				cortesQ.erase(cortesQ.begin());

				//copia optC a copyC
				//has los cortes que ya se han hecho.
				//trata de deshacer el cod.
				vector<vector<int>> copyC = optC;
				vector<int> copyCod = nodos_cod;
				for(int c = 0; c < corteT.size(); c++){
					int i = (int)corteT.at(c) / copyC.size();
					int j = corteT.at(c) % copyC.size();
					copyC.at(i).at(j) = 0;
					copyCod.erase(copyCod.begin() + getElemPosV(copyCod, j));
				}
				//(C, origen, padre, cod) cambia C y regresa el cod del padre cortado (padre*3+cod)
				int cut = trimCodNode(copyC, destinos, 0, padreT, nodos_cod.at(trimT));
				if(cut != -1){
					corteT.push_back(cut);
					copyC.at(padreT).at(nodos_cod.at(trimT)) = 0;
					copyCod.erase(copyCod.begin() + getElemPosV(copyCod, nodos_cod.at(trimT)));
				}

				//si no se ha tratado de cortar todos los cod. agrega a la cola los padres siguientes.
				// con trimT actual y pusheando el ultimo corte.
				trimT = trimT + 1;
				if(trimT < nodos_cod.size()){
					padres = getNodeParents(copyC, nodos_cod.at(trimT));
					parentQ.insert(parentQ.begin(), padres.begin(), padres.end());
					trimQ.insert(trimQ.begin(), padres.size(), trimT);
					cortesQ.insert(cortesQ.begin(), padres.size(), corteT);
				}

				//Busca los cortes más óptimos
				if(corteT.size() > maxTrim || (corteT.size() == maxTrim && optState != 1)){
					// system("pause");
					vector<vector<int>> packages = checkPackages(copyC, destinos, copyCod);
					pState = getState(packages, packages.size() - copyCod.size());
					if(pState != 0){
						if(optState == 0 || pState == 1 ||
											(pState == 2 && optState == 2 && corteT.size() > maxTrim)){
							maxTrim = corteT.size();
							optState = pState;
							optCortes = corteT;
							optPackages = packages;
						}
					}
				}

				// si maxTrim = numero de nodos a revisar(optState 1), se ha encontrado una ruta optima.
				if(maxTrim == nodos_cod.size() && optState == 1){
					opt = true;
				}
			}
		}

		//posible resultados:
		//opt = true ; se ha encontrado ruta optima maxima / minimo de codificadores paquetes simples
		//opt = false & maxTrim != 0 / se ha encontrado una ruta mejor con paquetes simples pero no borra todos los codificadores.
		//opt = false & maxTrim = 0 / no se ha encontrado una ruta mejor con paquetes simples, en este caso simplemente minimiza todos los cod porque de igual manera será con paquetes compuestos.

		//si maxTrim != 0 entonces se ha encontrado 1 solución mas óptima con pState 1
		if(maxTrim > 0){
			for(int c = 0; c < optCortes.size(); c++){
				int i = (int)optCortes.at(c) / optC.size();
				int j = optCortes.at(c) % optC.size();
				optC.at(i).at(j) = 0;
				nodos_cod.erase(nodos_cod.begin() + getElemPosV(nodos_cod, j));
			}

			Result << "Existen nodos codificadores redundantes \n";
			Result << "Grafo de Flujos Maximos con Redundancia eliminada: \n";
			Result << displayMatrix(optC, nNodos, nNodos);
			Result << "\n";
		}else{
			Result << "No Existen nodos codificadores redundantes \n\n";
		}
		if(optState != 0){
			Result << "Paquetes determinados para la solución del grafo:\n";
			int p = 0;
			int r = 0;
			while(r < (optPackages.size() - nodos_cod.size())){
				if(optC.at(0).at(p) == 1){
					Result << "S->N" << p << ": " << optPackages.at(r).at(0) << endl;
					r++;
				}
				p++;
			}
			Result << endl;
		}else{
			Result << "No se pudo determinar distribución de Paquetes para este grafo.\n\n";
		}

		//tempEnd
		cout << "\n DONE \n";
		Result << "\n";
		return optC;
		Result.close();

	}else{
 		cout << "Unable to open file";
	}
	// reimprime los arboles cortados
}

int main()
{
	string line;
	string matName = "18Nodes";
	ifstream Capacity("Matrices/"+matName+".mtx");
	int nNodos;
	vector<int> destinos;
	vector<vector<int>> capacidad;

	if(Capacity.is_open()){
		getline(Capacity, line);
		stringstream ss(line);
		string item;
		while(getline(ss, item, ' ')){
			destinos.push_back(stoi(item));
		}
		nNodos = destinos.at(0);
		destinos.erase(destinos.begin());

		while(getline(Capacity, line)){
			vector<int> col;
			stringstream ss(line);
			string item;
			while(getline(ss, item, ' ')){
				col.push_back(stoi(item));
			}
			capacidad.push_back(col);
		}
		Capacity.close();
	}else{
		cout << "Unable to open file\n";
	}

  vector<vector<int>> test = ffMulticast(nNodos, destinos, capacidad, matName);
}
