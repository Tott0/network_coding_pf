/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/netanim-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("butterfly-network");

uint32_t resource1,resource2;

void echoFromTo(uint32_t portServer, Ipv4Address addressServer, Ptr<Node> clientNode, double times [], uint32_t n){
	UdpEchoServerHelper echoServer(portServer);

	UdpEchoClientHelper echoClient(addressServer, portServer);;
	echoClient.SetAttribute ("MaxPackets", UintegerValue (n));
	echoClient.SetAttribute ("Interval", TimeValue (Seconds (0.0005)));
	echoClient.SetAttribute ("PacketSize", UintegerValue (128));

	ApplicationContainer clientApp = echoClient.Install (clientNode);
	clientApp.Start (Seconds (1.0));
	clientApp.Stop (Seconds (10.0));
}


void packetSent(Ptr< const Packet > packet)
{
	//std::cout<<Simulator::Now().GetSeconds()<<std::endl;
}

void scheduleUpDownInterfaces(Ptr<Node> n1, uint32_t packets){
	uint32_t NoPacket=0;
	double initialTime=1;
	double interval= 0.0005;
	double delay=0.002;
	double dataRate=0.0002;
	bool flag=true;

  	Ptr<Ipv4> ipv4n1 = n1->GetObject<Ipv4>();

	while(NoPacket<packets){
		if(flag){
			Simulator::Schedule(Seconds(initialTime+delay+dataRate),&Ipv4::SetDown, ipv4n1, 1);
			Simulator::Schedule(Seconds(initialTime+delay+dataRate),&Ipv4::SetUp  , ipv4n1, 2);
			flag=false;
		}else{
			Simulator::Schedule(Seconds(initialTime+delay+dataRate),&Ipv4::SetUp  , ipv4n1, 1);
			Simulator::Schedule(Seconds(initialTime+delay+dataRate),&Ipv4::SetDown, ipv4n1, 2);
			flag=true;
		}
		
		initialTime+=interval;
		NoPacket+=1;
	}
}

int main(int argc, char *argv[]){

	CommandLine cmd;
	uint32_t twoNPackets;
    cmd.AddValue ("twoNPackets", "Number of \"packets\" in the multicast", twoNPackets);
    cmd.Parse (argc,argv);

    (twoNPackets<=0)? twoNPackets=2 : twoNPackets *= 2;

	//Time::SetResolution (Time::NS);
	LogComponentEnable ("OnOffApplication", LOG_LEVEL_INFO);
	LogComponentEnable ("PacketSink", LOG_LEVEL_INFO);
	LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);

	//Si se baja o sube una interfaz inmediatamente se informa
	//de este evento
	Config::SetDefault ("ns3::Ipv4GlobalRouting::RespondToInterfaceEvents", BooleanValue (true));

	//3 contenedores cada uno con 2 nodos
	NodeContainer t0r0,t1r5,t2r6;
	t0r0.Create(2);
	t1r5.Create(2);
	t2r6.Create(2);

	//9 contenedores y agregamos un nodo de cada contenedor
	//previamente creado 
	NodeContainer r0r1, r0r2, r1r3, r2r3,
	r1r5, r2r6, r3r4, r4r5, r4r6;

	r0r1.Add(t0r0.Get(1));
	r0r1.Create(1);
	

	r0r2.Add(t0r0.Get(1));
	r0r2.Create(1);

	r1r3.Add(r0r1.Get(1));
	r1r3.Create(1);

	r2r3.Add(r0r2.Get(1));
	r2r3.Add(r1r3.Get(1));

	r3r4.Add(r1r3.Get(1));
	r3r4.Create(1);

	r1r5.Add(r1r3.Get(0));
	r1r5.Add(t1r5.Get(1));

	r2r6.Add(r2r3.Get(0));
	r2r6.Add(t2r6.Get(1));

	r4r5.Add(r3r4.Get(1));
	r4r5.Add(t1r5.Get(1));

	r4r6.Add(r3r4.Get(1));
	r4r6.Add(t2r6.Get(1));

	//conexion PTPHelper que sera utilizada en cada conexion
	PointToPointHelper pointToPoint;
	pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
	pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));
	//NetDevice para cada una de estas conexiones
	NetDeviceContainer t0r0devices = pointToPoint.Install(t0r0);
	NetDeviceContainer t1r5devices = pointToPoint.Install(t1r5);
	NetDeviceContainer t2r6devices = pointToPoint.Install(t2r6);
	NetDeviceContainer r0r1devices = pointToPoint.Install(r0r1);
	NetDeviceContainer r0r2devices = pointToPoint.Install(r0r2);
	NetDeviceContainer r1r3devices = pointToPoint.Install(r1r3);
	NetDeviceContainer r2r3devices = pointToPoint.Install(r2r3);
	NetDeviceContainer r1r5devices = pointToPoint.Install(r1r5);
	NetDeviceContainer r2r6devices = pointToPoint.Install(r2r6);
	NetDeviceContainer r3r4devices = pointToPoint.Install(r3r4);
	NetDeviceContainer r4r5devices = pointToPoint.Install(r4r5);
	NetDeviceContainer r4r6devices = pointToPoint.Install(r4r6);

	//stack de aplicaciones en cada nodo sin repetir
	InternetStackHelper stack;
	stack.InstallAll();

	//ip para cada dispositivos 
	Ipv4AddressHelper address;
	address.SetBase("10.1.1.0","255.255.255.0");
	Ipv4InterfaceContainer r0r1Interfaces = address.Assign(r0r1devices);

	address.SetBase("10.1.2.0","255.255.255.0");
	Ipv4InterfaceContainer r0r2Interfaces = address.Assign(r0r2devices);

	address.SetBase("10.1.3.0","255.255.255.0");
	Ipv4InterfaceContainer r1r3Interfaces = address.Assign(r1r3devices);

	address.SetBase("10.1.4.0","255.255.255.0");
	Ipv4InterfaceContainer r2r3nterfaces = address.Assign(r2r3devices);

	address.SetBase("10.1.5.0","255.255.255.0");
	Ipv4InterfaceContainer r1r5Interfaces = address.Assign(r1r5devices);

	address.SetBase("10.1.6.0","255.255.255.0");
	Ipv4InterfaceContainer r2r6Interfaces = address.Assign(r2r6devices);

	address.SetBase("10.1.7.0","255.255.255.0");
	Ipv4InterfaceContainer r3r4Interfaces = address.Assign(r3r4devices);

	address.SetBase("10.1.8.0","255.255.255.0");
	Ipv4InterfaceContainer r4r5Interfaces = address.Assign(r4r5devices);

	address.SetBase("10.1.9.0","255.255.255.0");
	Ipv4InterfaceContainer r4r6Interfaces = address.Assign(r4r6devices);

	address.SetBase("192.168.1.0","255.255.255.0");
	Ipv4InterfaceContainer t0r0Interfaces = address.Assign(t0r0devices);

	address.SetBase("192.168.2.0","255.255.255.0");
	Ipv4InterfaceContainer t1r5Interfaces = address.Assign(t1r5devices);

	address.SetBase("192.168.3.0","255.255.255.0");
	Ipv4InterfaceContainer t2r6Interfaces = address.Assign(t2r6devices);

	Ipv4Address multicastSource  ("192.168.1.1");
	Ipv4Address multicastGroup ("225.1.2.4");

	Ipv4StaticRoutingHelper multicast;

	//First Sender
	Ptr<Node> sender = t0r0.Get(0); //n0
	Ptr<NetDevice> senderIf = t0r0devices.Get (0);
	multicast.SetDefaultMulticastRoute (sender, senderIf);

	Ptr<Node> n1MulticastRouter=t0r0.Get(1); //n1
	Ptr<NetDevice> n1InputIf=t0r0devices.Get(1);
	NetDeviceContainer n1OutputDevices;
	n1OutputDevices.Add(r0r1devices.Get(0));
	n1OutputDevices.Add(r0r2devices.Get(0));

	multicast.AddMulticastRoute (n1MulticastRouter, multicastSource, 
		multicastGroup, n1InputIf, n1OutputDevices);

	//Rama izquiera
	NetDeviceContainer n6OutputDevices;
	n6OutputDevices.Add(r1r3devices.Get(0));
	n6OutputDevices.Add(r1r5devices.Get(0));

	multicast.AddMulticastRoute(r0r1.Get(1), multicastSource,
		multicastGroup, r0r1devices.Get(1), n6OutputDevices);

	NetDeviceContainer n3OutputDevices;
	n3OutputDevices.Add(t1r5devices.Get(1));

	multicast.AddMulticastRoute(r1r5.Get(1), multicastSource,
		multicastGroup, r1r5devices.Get(1), n3OutputDevices);
	multicast.AddMulticastRoute(r1r5.Get(1), multicastSource,
		multicastGroup, r4r5devices.Get(1), n3OutputDevices);
	//medio
	NetDeviceContainer n8OutputDevices;
	n8OutputDevices.Add(r3r4devices.Get(0));
	multicast.AddMulticastRoute(r3r4.Get(0), multicastSource,
		multicastGroup, r1r3devices.Get(1), n8OutputDevices);

	multicast.AddMulticastRoute(r3r4.Get(0), multicastSource,
		multicastGroup, r2r3devices.Get(1), n8OutputDevices);

	NetDeviceContainer n9OutputDevices;
	n9OutputDevices.Add(r4r5devices.Get(0));
	n9OutputDevices.Add(r4r6devices.Get(0));

	multicast.AddMulticastRoute(r3r4.Get(1), multicastSource,
		multicastGroup, r3r4devices.Get(1), n9OutputDevices);

	//Rama derecha
	NetDeviceContainer n7OutputDevices;
	n7OutputDevices.Add(r2r3devices.Get(0));
	n7OutputDevices.Add(r2r6devices.Get(0));

	multicast.AddMulticastRoute(r0r2.Get(1), multicastSource,
		multicastGroup, r0r2devices.Get(1), n7OutputDevices);

	NetDeviceContainer n5OutputDevices;
	n5OutputDevices.Add(t2r6devices.Get(1));

	multicast.AddMulticastRoute(t2r6.Get(1), multicastSource,
		multicastGroup, r2r6devices.Get(1), n5OutputDevices);

	multicast.AddMulticastRoute(t2r6.Get(1), multicastSource,
		multicastGroup, r4r6devices.Get(1), n5OutputDevices);

	uint16_t multicastPort = 9;   // Discard port (RFC 863)

	// Configure a multicast packet generator that generate a packet

	double times [] = {1.0 , 10.0 };
	echoFromTo( multicastPort, multicastGroup, t0r0.Get(0), times, twoNPackets);
	//times[0] = 1.0005;
	//echoFromTo( multicastPort, multicastGroup, a0r0.Get(0), times);

	PacketSinkHelper sink ("ns3::UdpSocketFactory",
		InetSocketAddress (Ipv4Address::GetAny (), multicastPort));

	ApplicationContainer sinkn2 = sink.Install (t1r5.Get(0)); // Node n2 
	//Start the sink
	sinkn2.Start (Seconds (1.0));
	sinkn2.Stop (Seconds (10.0));

	ApplicationContainer sinkn4 = sink.Install (t2r6.Get(0)); // Node n4 
	sinkn4.Start (Seconds (1.0));
	sinkn4.Stop (Seconds (10.0));

	//pointToPoint.EnablePcapAll ("butterfly-network/butterfly-network");
	//AsciiTraceHelper ascii;
  	//pointToPoint.EnableAsciiAll(ascii.CreateFileStream("butterfly-network/butterfly-network.tr"));

  	//Obtenemos el DataRate del pointToPointNetDevice asociado a t0r0
  	//DataRateValue dr;
  	//Ptr<Object> netDevice = t0r0devices.Get(0);
  	//Ptr<PointToPointNetDevice> ppNetDevice = netDevice->GetObject<PointToPointNetDevice>();
  	//ppNetDevice->GetAttribute("DataRate", dr);
  	//std::cout<<Seconds(dr.Get().GetBitRate())<<std::endl;
  	//std::cout<<MilliSeconds(dr.Get().CalculateBytesTxTime(uint32_t(128)))<<std::endl;

  	//Obtenemos el delay del channel asociado a t0r0
  	//TimeValue tv;
  	//Ptr<Object> channelNetDevice = t0r0devices.Get(0)->GetChannel();
  	//Ptr<PointToPointChannel> ppChannel= channelNetDevice->GetObject<PointToPointChannel>();
  	//ppChannel->GetAttribute("Delay", tv);
  	//std::cout<<tv.Get().GetSeconds()<<std::endl;//0.002


  	//N0 Queue 
  	//Ptr<Queue> queue = ppNetDevice->GetQueue();

  	scheduleUpDownInterfaces(t0r0.Get(1), twoNPackets);

	/*Ptr<Node> n1 = t0r0.Get(1);
  	Ptr<Ipv4> ipv4n1 = n1->GetObject<Ipv4>();
  	//uint32_t ipv4ifIndex3 = 3;

  	Simulator::Schedule (Seconds (1.002 ),&Ipv4::SetDown, ipv4n1, 1);
  	Simulator::Schedule (Seconds (1.0026),&Ipv4::SetUp  , ipv4n1, 1);
  	Simulator::Schedule (Seconds (1.0025),&Ipv4::SetDown, ipv4n1, 2);
  	Simulator::Schedule (Seconds (1.0028),&Ipv4::SetUp  , ipv4n1, 2);*/

	//posicionamos los nodos 
	AnimationInterface::SetConstantPosition(t0r0.Get(0), 6.0, -8.0);
	AnimationInterface::SetConstantPosition(t0r0.Get(1), 6.0, 1.0);
	AnimationInterface::SetConstantPosition(r0r1.Get(1), -1.0, 6.5);
	AnimationInterface::SetConstantPosition(r0r2.Get(1), 13.0, 6.5);
	AnimationInterface::SetConstantPosition(r1r3.Get(1), 6.0, 11.0);
	AnimationInterface::SetConstantPosition(r3r4.Get(1), 6.0, 17.5);
	AnimationInterface::SetConstantPosition(t1r5.Get(1), -1.0, 25.0);
	AnimationInterface::SetConstantPosition(t1r5.Get(0), -1.0, 39.0);
	AnimationInterface::SetConstantPosition(t2r6.Get(1), 13.0, 25.0);
	AnimationInterface::SetConstantPosition(t2r6.Get(0), 13.0, 39.0);

	AnimationInterface animation("multicastclasico.xml");

	Config::ConnectWithoutContext ("/NodeList/0/ApplicationList/*/$ns3::UdpEchoClient/Tx", MakeCallback (&packetSent));

	
  // Ptr<FlowMonitor> flowmon;
  // FlowMonitorHelper flowmonHelper;
  // flowmon = flowmonHelper.InstallAll ();


	//obtenemos las imagenes 
	//nota: Cambiar la direccion en donde se encuentren las imagenes
	resource1 = animation.AddResource("/media/sf_sf_centos_files/repos/ns-allinone-3.23/ns-3.23/scratch/router.png");
	resource2 = animation.AddResource("/media/sf_sf_centos_files/repos/ns-allinone-3.23/ns-3.23/scratch/monitor2.png");

	//Cambiamos la imagen de los nodos
	//Primero los endpoints
	animation.UpdateNodeImage(t0r0.Get(0)->GetId(), resource2);
	animation.UpdateNodeImage(t1r5.Get(0)->GetId(), resource2);
	animation.UpdateNodeImage(t2r6.Get(0)->GetId(), resource2);

	//Luego los routers
	animation.UpdateNodeImage(t0r0.Get(1)->GetId(), resource1);
	animation.UpdateNodeImage(t1r5.Get(1)->GetId(), resource1);
	animation.UpdateNodeImage(t2r6.Get(1)->GetId(), resource1);
	animation.UpdateNodeImage(r0r1.Get(1)->GetId(), resource1);
	animation.UpdateNodeImage(r0r2.Get(1)->GetId(), resource1);
	animation.UpdateNodeImage(r1r3.Get(1)->GetId(), resource1);
	animation.UpdateNodeImage(r3r4.Get(1)->GetId(), resource1);

	//Change the color of the terminal
	animation.UpdateNodeColor(t0r0.Get(0), 122, 186, 122);
	animation.UpdateNodeColor(t1r5.Get(0), 122, 186, 122);
	animation.UpdateNodeColor(t2r6.Get(0), 122, 186, 122);


	//Cambiamos el tamaño de los endpoints
	animation.UpdateNodeSize(t0r0.Get(0)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(t1r5.Get(0)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(t2r6.Get(0)->GetId(), 6.0, 6.0);

	//Luego los routers
	animation.UpdateNodeSize(t0r0.Get(1)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(t1r5.Get(1)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(t2r6.Get(1)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(r0r1.Get(1)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(r0r2.Get(1)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(r1r3.Get(1)->GetId(), 6.0, 6.0);
	animation.UpdateNodeSize(r3r4.Get(1)->GetId(), 6.0, 6.0);




	Simulator::Run ();

	Simulator::Destroy ();
	return 0;

}
